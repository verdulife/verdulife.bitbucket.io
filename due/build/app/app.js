app.app = ($scope, $route, $location, $timeout, $window, $sce, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage) => {

    $scope.modal = false;
    $scope.filterTask = 'verdu';
    $scope.newBtn = false;
    $scope.editBtn = false;

    $scope.refresh = () => {
        $route.reload();
    };

    $scope.switchFilter = () => {
        $('#tableSelect').blur();
        window.focus();
        let selectValue = $('#tableSelect').val();
        if (selectValue == 'verdu') {
            $scope.filterTask = 'archivado'
        } else {
            $scope.filterTask = 'verdu'
        }
    }

    let vW = $(window).width();
    if (vW >= 860) {
        $scope.mobile = false;
        $scope.isMobile = 'entrega';
    } else {
        $scope.mobile = true;
        $scope.isMobile = '-entrega';
    }

    $scope.showModal = () => {
        $scope.modal = true;
        $scope.newBtn = true;
        $scope.trabajador = 'verdu';
        $scope.entrega = new Date();

        $timeout(() => {
            $('.first-input').focus();
        }, 500);
    };

    $scope.hideModal = () => {
        $scope.modal = false;
        $scope.newBtn = false;
        $scope.editBtn = false;
        $scope.edit = null;
        $scope.cliente = null;
        $scope.desc = null;
        $scope.trabajador = null;
        $scope.entrega = null;
        $scope.file = null;
    };

    $scope.switchImg = (id) => {
        $('#' + id).toggleClass('full-img');
        $('#' + id).siblings().toggle();
    }

    /* var recognition;
    var recognizing = false;

    if (!('webkitSpeechRecognition' in window)) {

        console.log("¡Atención! El reconocimiento de voz no esta soportado por este navegador");

    } else {

        recognition = new webkitSpeechRecognition();
        recognition.lang = "es-Es";
        recognition.continuous = true;
        recognition.interimResults = true;

        recognition.onstart = () => {
            recognizing = true;
            console.log("Escuchando...");
        };

        recognition.onresult = (event) => {
            for (var i = event.resultIndex; i < event.results.length; i++) {
                if (event.results[i].isFinal)
                    $(".desc").val(event.results[i][0].transcript);
                $scope.desc = event.results[i][0].transcript;
            }
        };

        recognition.onerror = (event) => {
            console.log("error escuchando");
        }
        recognition.onend = () => {
            recognizing = false;
            $(".mic-icon").css('opacity', '1');
            console.log("terminó de escuchar, llegó a su fin");
        };

    };

    $scope.procesar = () => {
        if (recognizing == false) {
            $(".desc").val('');
            recognition.start();
            recognizing = true;
            $(".mic-icon").css('opacity', '.5');
        } else {
            recognition.stop();
            recognizing = false;
            $(".mic-icon").css('opacity', '1');
        }
    }; */
}