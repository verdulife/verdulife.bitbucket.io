app.appFirebase = function ($scope, $location, $timeout, $window, $sce, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage) {
    const auth = $firebaseAuth();
    const db = firebase.firestore();

    $scope.signOut = () => {
        auth.$signOut();
        $location.path('/login');
    }

    // refs
    const ordenesRef = db.collection('ordenes');
    const storageRef = firebase.storage().ref('ordenes');

    //realtime db
    ordenesRef.onSnapshot((querySnapshot) => {
        var ordenes = [];
        querySnapshot.forEach((doc) => {
            ordenes.push(doc.data());
        });
        $timeout(() => {
            $scope.ordenes = ordenes;
        });
    });

    //get files
    $scope.readFile = function (event) {
        let file = event.currentTarget.files[0];
        let reader = new FileReader();
        
        reader.onloadend = function () {
            $scope.file = reader.result;
        }

        if (file) {
            reader.readAsDataURL(file);
        } else {
            console.log('no file attached');
        }
    };

    $scope.deleteImg = () => {
        $scope.file = '';
    }

    //create order
    $scope.newOrder = function () {

        ordenesRef.add({

            cliente: this.cliente,
            trabajador: this.trabajador,
            entrega: this.entrega,
            file: this.file || '',
            desc: this.desc,
            date: new Date()

        }).then(function (docRef) {

            ordenesRef.doc(docRef.id).update({
                id: docRef.id
            });

        });

        $scope.hideModal();

    };

    $scope.editOrder = function (orden) {

        $scope.modal = true;
        $scope.newBtn = false;
        $scope.editBtn = true;

        ordenesRef.doc(orden.id).get().then(function (doc) {
            $timeout(function () {
                $scope.edit = doc.data();
                $scope.cliente = $scope.edit.cliente;
                $scope.trabajador = $scope.edit.trabajador || '';
                $scope.entrega = $scope.edit.entrega || '';
                $scope.file = $scope.edit.file || '';
                $scope.desc = $scope.edit.desc || '';
            });
        })
    }

    $scope.changeOrder = function () {

        ordenesRef.doc($scope.edit.id).update({
            cliente: this.cliente,
            trabajador: this.trabajador,
            entrega: this.entrega,
            file: this.file,
            desc: this.desc,
            date: new Date()
        });

        $scope.hideModal();

    };

    //user state
    auth.$onAuthStateChanged(function (firebaseUser) {

        if (firebaseUser) {

            //remove order
            $scope.removeOrder = function (orden) {
                var sure = window.confirm('¿Seguro que quieres borrar?');
                if (sure) {
                    console.log(orden.id + ' borrada');
                    ordenesRef.doc(orden.id).delete();
                } else {
                    console.log(orden.id + ' no borrada');
                }
            }

            $scope.checkOrder = function (orden) {
                var sure = window.confirm('¿Seguro que quieres archivar?');
                if (sure) {
                    console.log(orden.id + ' archivada');
                    ordenesRef.doc(orden.id).update({
                        trabajador: 'archivado'
                    })
                } else {
                    console.log(orden.id + ' no archivada');
                }
            }

            $scope.autoRemove = (orden) => {
                let twoAgo = new Date().getMonth() - 1;
                let ordenDate = orden.date.getMonth() + 1;

                if (orden.trabajador == 'archivado' && ordenDate <= twoAgo) {
                    alert(orden.cliente, 'Fue creado hace más de 2 meses, se tendria que borrar');
                    /* ordenesRef.doc(orden.id).delete(); */
                } 
            }

        }

    });

}