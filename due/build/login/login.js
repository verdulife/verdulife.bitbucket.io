app.login = function ($scope, $location, $timeout, $window, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage) {

  $scope.signForm = true;
  $scope.newForm = false;

  $scope.toggleForm = () => {
    $scope.signForm = $scope.signForm ? false : true;
    $scope.newForm = $scope.newForm ? false : true;
  }

  $timeout(() => {
    $('.first-input').focus();
  });

}