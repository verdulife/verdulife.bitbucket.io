app.loginFirebase = function ($scope, $location, $timeout, $window, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage) {
  var auth = $firebaseAuth();
  var db = firebase.firestore();
  $scope.form = $scope;

  $scope.signIn = function () {
    auth.$signInWithEmailAndPassword($scope.name, $scope.pass)
      .then(function (firebaseUser) {
        console.log('Usuario: ' + $scope.name + ' conectado');

      }).catch(function (error) {
        console.error(error)
      });
  };

  // user !connected ? connected
  auth.$onAuthStateChanged(function (firebaseUser) {

    if (firebaseUser) {
      console.log('Usuario conectado. Redirigiendo...')
      $location.path('/app');

    } else {
      console.log('Usuario no conectado');

    }
  });

}