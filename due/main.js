//app name
const app = angular.module('app', ['ngRoute', 'ngAnimate', 'firebase', 'luegg.directives']);

//firebase auth
const config = {
    apiKey: "AIzaSyD9DRrg7Kcci8-9V9WNOAPl3H5dLIVHcro",
    authDomain: "dueapp-task.firebaseapp.com",
    databaseURL: "https://dueapp-task.firebaseio.com",
    projectId: "dueapp-task",
    storageBucket: "dueapp-task.appspot.com",
    messagingSenderId: "213811036969"
};

firebase.initializeApp(config);

app.factory("Auth", ["$firebaseAuth", ($firebaseAuth) => {
    return $firebaseAuth();
}]);

// !auth redirections
app.run(["$rootScope", "$location", ($rootScope, $location) => {
    $rootScope.$on("$routeChangeError", (event, next, previous, error) => {
        if (error === "AUTH_REQUIRED") {
            $location.path("/login");
        }
    });
}]);

let today = new Date().getDate();

//filters
app.filter('html', function($sce) {
    return function(textarea) {
        return $sce.trustAsHtml(textarea.replace(/\n/g, "<br>"));
    }
});

app.filter('humandate', function() {
    return function(date) {
        date = date.getDate();
        if (date < today) {
            return 'Excedido';
        } else if (date == today) {
            return 'Hoy';
        } else if (date == (today + 1)) {
            return 'Mañana';
        } else {
            return date
        }
    }
});

//on-enter
app.directive('onEnter', () => {
    return (scope, element, attrs) => {
        element.bind("keydown keypress", (event) => {
            if (event.which === 13) {
                scope.$apply(() => {
                    scope.$eval(attrs.onEnter);
                });
                event.preventDefault();
            }
        });
    };
});

//on-esc
app.directive('onEsc', () => {
    return (scope, element, attrs) => {
        element.bind("keydown keypress", (event) => {
            if (event.which === 27) {
                scope.$apply(() => {
                    scope.$eval(attrs.onEsc);
                });
                event.preventDefault();
            }
        });
    };
});

//on-alt
app.directive('onAlt', () => {
    return (scope, element, attrs) => {
        element.bind("keydown keypress", (event) => {
            if (event.which === 18) {
                scope.$apply(() => {
                    scope.$eval(attrs.onEsc);
                });
                event.preventDefault();
            }
        });
    };
});

//$locations
app.config(['$routeProvider', ($routeProvider) => {

    $routeProvider
        .when('/login', {
            templateUrl: './build/login/login.html',
            controller: 'appController',
        })
        .when('/app', {
            templateUrl: './build/app/app.html',
            controller: 'appController',
            // !auth ? auth
            resolve: {
                "currentAuth": ["Auth", (Auth) => {
                    return Auth.$requireSignIn();
                }]
            }
        })
        .otherwise({
            //404?
            redirectTo: '/login',
        });

}]);

//js source
app.controller('appController', ['$scope', '$route', '$location', '$window', '$timeout', '$sce', '$firebaseObject', '$firebaseArray', '$firebaseAuth', '$firebaseStorage',
    function ($scope, $route, $location, $window, $timeout, $sce, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage) {

        //login
        app.login($scope, $location, $timeout, $window, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage);
        app.loginFirebase($scope, $location, $timeout, $window, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage);

        //app
        app.app($scope, $route, $location, $timeout, $window, $sce, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage);
        app.appFirebase($scope, $location, $timeout, $window, $sce, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage);

    }
]);